/*Functions, libraries and relevant constants are declared here*/

#include <stdio.h>
#include <dirent.h>
#include <limits.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <pwd.h>
#include <sys/types.h>
#include <sys/stat.h>
// Max file size allowed to use view
#define MAX_SIZE 500000
// Follows are unicode characters
#define DIRECTORY "{</#x1F5C0>}"
#define EXEC "{</#x1F4BE>}" 
#define TEXT "{</#x1F4C4>}"

int printDirContent(char *dir);
int changeDirAndPWD(char *dir);
int printCurDir();
void execute(char *path);
void view(char *path);
void renameItem(char *path);
void showMenu(char *path);
void printFileInfo(char *path);