/* Implementation of functions used by the main driver*/

#include "function.h"

int printDirContent(char *dir)
{
    struct dirent *de;  // Pointer for directory entry
 
    // opendir() returns a pointer of DIR type. 
    DIR *dr = opendir(dir);
 
    if (dr == NULL)  // opendir returns NULL if couldn't open directory
    {
        printf("<errorMessage.visible true>\
        <errorMessage {Error: You don't have permission to view this directory!}>");
        return 0;
    }
 
    // Refer http://pubs.opengroup.org/onlinepubs/7990989775/xsh/readdir.html
    // for readdir()
    // Iteratively read each item in the directory
    while ((de = readdir(dr)) != NULL)
    {
        //Don't display . and .. since they're hidden
        if ((strcmp(de->d_name,".") != 0) && (strcmp(de->d_name,"..") != 0))
        {
            // Categorize the item and set the appropriate onclick command
            char fileType[16];
            char onClickCommand[NAME_MAX+3];
            char onContextClickCommand[NAME_MAX+3]="c:";
            if (de->d_type == 4) 
            {
                strcpy(fileType,DIRECTORY);
                strcpy(onClickCommand,"d:");
            }
            else if ((de->d_type == 8) && !access(de->d_name,X_OK))
            {
                strcpy(fileType,EXEC);
                strcpy(onClickCommand,"x:");
            }
            else 
            {
                strcpy(fileType,TEXT);
                strcpy(onClickCommand,"v:");
            }
            strcat(onClickCommand,de->d_name);
            strcat(onContextClickCommand,de->d_name);
            // Spawn a box, containing the type image, file name and onclick listener
            // Right click triggers menu, hides error box, resets info box
            printf("<box inline=true spacing-right=20 spacing-top=10\
            oncontextclick=<putln {%s}>, <errorMessage.visible false>,\
            <infoBox {Click for file property}> onclick=<putln {%s}>\
            border=0 color={black} <box size=60 border=0 bold=true color={green} text-align={center} %s>\
            size=20 width=30,{%} {%s} >",onContextClickCommand,onClickCommand,fileType,de->d_name);
        }
    }
    printf("\n");
    closedir(dr);    
    return 0;
}

int changeDirAndPWD(char *dir)
{
    // Error when change dir
    if (chdir(dir))
    {
        if (errno == EACCES) printf("<errorMessage.visible true>\
        <errorMessage {Error: You don't have permission to view this directory!}>");
        return 0;
    }
    char cwd[PATH_MAX];
    getcwd(cwd,sizeof(cwd));
    setenv("PWD",cwd,1);
    return 1;
}


int printCurDir()
{
    char cwd[PATH_MAX];
    if (getcwd(cwd,sizeof(cwd)) != NULL) 
    {
        printf("<curDir.clear>");
        printf("<curDir {Current Directory: %s}>",cwd);
        printf("</n>");
        return 0;
    }
    return 1;

}

void execute(char *path)
{
    if (!access(path,X_OK))
    {
        char fullPath[PATH_MAX+2] ="./";
        strcat(fullPath,path); // Complete the path to the file
        char command[PATH_MAX+16] = "xterm -e ";
        strcat(command,fullPath); // Concat the path to the command
        strcat(command," &"); // Run program in the background to not block the current process

        if (system(command) == -1) printf("<errorMessage.visible true>\
        <errorMessage {Error: File run unsuccessfully!}>");       

    }
    
    else if (errno == EACCES) printf("<errorMessage.visible true>\
    <errorMessage {Error: You don't have permission to execute this file!}>");
}

void view(char *path)
{
    if (!access(path,R_OK))
    {
        // Check if file small enough
        struct stat statBuf;
        stat(path,&statBuf);
        if (statBuf.st_size <= MAX_SIZE)
        {
            char command[PATH_MAX+16] = "xterm -e less ";
            strcat(command,path); // Concat the path to the command
            strcat(command," &"); // View the file in the background

            if (system(command) == -1) printf("<errorMessage.visible true>\
            <errorMessage {Error: File viewed unsuccessfully!}>");       
        }
        else printf("<errorMessage.visible true>\
        <errorMessage {Error: File is too big to be viewed this way!}>");
    }
    else if (errno == EACCES) printf("<errorMessage.visible true>\
    <errorMessage {Error: You don't have permission to view this file!}>");
}


void renameItem(char *path)
{
    //user type name, push input back to C, read it, rename
    char newName[NAME_MAX];
    printf("<putln <renameBox>>");
    scanf("%s",newName);


    // If rename ok, draw dir again
    if (!rename(path,newName))
    {
        printf("<putln {d:.}>");
    }
    else if (errno == EACCES) printf("<errorMessage.visible true>\
    <errorMessage {Error: You don't have permission to modify this file/ folder!}>");   
}

void showMenu(char *path) // fires when context click
{
    printf("<renameBox.visible true>");
    printf("<renameBox {%s selected. To rename, enter the new name here and confirm.}>",path);

    printf("<confirmBox.visible true>");
    printf("<infoBox.visible true>");

    printf("<item {%s}>",path); // assign current file to click listener
}

void printFileInfo(char *path)
{
    struct stat statbuf;

    stat(path,&statbuf); // Get info from stat()

    // Then show info if it's a file
    if (S_ISREG(statbuf.st_mode))
    {
        struct passwd *pwd;
        // Assume we always find username of owner
        pwd = getpwuid(statbuf.st_uid);
        long fileSize = statbuf.st_size/1000;

        // Get permission
        char filePermission[4] = "";
        if (statbuf.st_mode & S_IRUSR) strcat(filePermission,"R");
        if (statbuf.st_mode & S_IWUSR) strcat(filePermission,"W");
        if (statbuf.st_mode & S_IXUSR) strcat(filePermission,"X");


        printf("<infoBox {File name: %s | Owner name: %s | File size: %ld KBytes | Owner permission: %s}>"
        ,path,pwd->pw_name,fileSize,filePermission);
    }
    else printf("<errorMessage.visible true>\
    <errorMessage {Error: Can only inspect files not directory!}>");
    

}