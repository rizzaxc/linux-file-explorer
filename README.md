# Graphical shell for Linux
A simple user interface written in C for Linux. It has very limited functionalities.
![Screenshot of the GUI](showcase/img3.png)

## Getting started

### Prerequisites
You have to have a working ioL environment in order for the graphical UI to work correctly.
Unfortunately, the source code for it is not available (the GUI was originally developed in a VM).
For further information regarding ioL, contact [Daniel Kos](https://gitlab.com/danielkos/).

### Functionalities:
- View all items in an directory
- Change directory
- Execute a file with permission in xterm
- View a file using less
- Rename and inspect a file/ folder

### Operation:
- One left-click on an item will trigger the appropriate response
- Additionally, use right-click on an item to display the extra functionalies 
    

### Assumption and Limitation:
- If a file is larger than 500kb, it's too large to view with less
- All files with execution permission are considered executable, graphically marked with a floppy disk
- Permission info is for the owner only, due to limited space
- If you rename a file to an existing name, the file with the name will be overwritten without notice
- Design is not responsive

## Misc

### Author:
- **Thach Kim Tran**: sole author. This was originally a project for a university assignment.

### License:
This project is licensed under the [MIT License](https://opensource.org/licenses/MIT).